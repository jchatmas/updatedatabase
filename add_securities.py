#!/usr/bin/python3.6

import pandas as pd
import psycopg2
import numpy as np


def getDataCols(dbName, dbTable):
    conn = psycopg2.connect('dbname={} user=wesbdip password=waterloo2016'.format(dbName))
    cur = conn.cursor()
    cur.execute('SELECT * FROM {};'.format(dbTable))
    data = cur.fetchall()
    cols = [col[0] for col in cur.description]
    df = pd.DataFrame(data, columns=cols)
    cur.close()
    conn.close()
    return df, cols


# Creates a string separated by spaces from a list
def stringFromList(Li):
    return '(' + ' '.join(x for x in Li) + ')'


# Creates a string version of a SQL insert query from a dataframe
def stringInsertQuery(df):
    columns = list(df.columns.values)
    for i in range(len(columns)-1):
        columns[i] = str(columns[i]) + ','
    columns = stringFromList(columns)
    stringFormat = list('%s' for x in df.columns.values)
    for i in range(len(stringFormat)-1):
        stringFormat[i] = stringFormat[i] + ','
    stringFormat = stringFromList(stringFormat)
    return columns, stringFormat


def insertQuery(df, dbName, dbTable, dbUser, dbPassword):
    conn = psycopg2.connect('dbname={} user={} password={}'.format(dbName, dbUser, dbPassword))
    cur = conn.cursor()
    columns, stringFormat = stringInsertQuery(df)
    for i in df.index.values:
        rowValues = []
        for c in df.columns.values:
            if df.loc[i, c] == 'f':
                rowValues.append(False)
            elif df.loc[i, c] == 't':
                rowValues.append(True)
            elif pd.isnull(df.loc[i, c]):
                rowValues.append(None)
            elif type(df.loc[i, c]) == np.int64:
                rowValues.append(int(df.loc[i, c]))
            elif type(df.loc[i, c]) == np.float64:
                rowValues.append(float(df.loc[i, c]))
            else:
                rowValues.append(df.loc[i, c])
        rowValues = tuple(rowValues)
        querystring = 'INSERT INTO ' + dbTable + ' ' + columns + ' VALUES ' + stringFormat
        cur.execute(querystring, rowValues)
    conn.commit()
    cur.close()
    conn.close()


if __name__ == '__main__':
    sec, secCols = getDataCols('modeldb', 'security')
    etf, etfCols = getDataCols('modeldb', 'etf_universe')
    stock, stockCols = getDataCols('modeldb', 'stock_universe')
    etfCols = [x for x in etfCols if x in secCols]
    stockCols = [x for x in stockCols if x in secCols]
    etf = etf[etfCols]
    stock = stock[stockCols]
    stock['asset_class'] = 'Equity'
    
    stock = stock[~stock['ticker'].isin(sec.ticker.values)]
    etf = etf[~etf['ticker'].isin(sec.ticker.values)]

    if len(stock) > 0:
        insertQuery(stock, 'modeldb', 'security', 'wesbdip', 'waterloo2016')
        print('Inserted ', len(stock), ' stocks into security table.')
    else:
        print('No stocks to insert')

    if len(etf) > 0:
        insertQuery(etf, 'modeldb', 'security', 'wesbdip', 'waterloo2016')
        print('Inserted ', len(etf), ' etfs into security table.')
