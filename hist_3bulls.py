#!/usr/bin/python3.6

import pandas as pd
import numpy as np
import psycopg2

path = '/home/wesbdip/temp_dumps/'

conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
cur = conn.cursor()
query = '''SELECT time, ticker, abs_100day_gmdh, abs_100week_gmdh, abs_100month_gmdh
FROM gmdh_hist WHERE ticker = %s'''
cur.execute(query, ('SPY',))
data = cur.fetchall()
cols = [col[0] for col in cur.description]
df = pd.DataFrame(data, columns=cols)
df['abs_100day_gmdh'] = (df['abs_100day_gmdh'] / 2)
df['abs_100day_gmdh'] = df['abs_100day_gmdh'].astype(np.int64)
df['bulls'] = df.sum(axis=1)
cur.close()
conn.close()

fname = path + 'hist_bulls.csv'
df.to_csv(fname, index=False)
