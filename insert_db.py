#!/usr/bin/python3.6

import pandas as pd
import numpy as np
import psycopg2

path = '/home/wesbdip/temp_dumps/'

inFile = str(input('Enter filename of the csv you want to insert: '))
dbName = str(input('Enter the name of the target database: '))
dbTable = str(input('Enter the name of the target table: '))
dbUser = str(input('Enter the database username: '))
dbPassword = str(input('Enter the database password '))
fName = path + inFile

df = pd.read_csv(fName)


# Creates a string separated by spaces from a list
def stringFromList(Li):
    return '(' + ' '.join(x for x in Li) + ')'


# Creates a string version of a SQL insert query from a dataframe
def stringInsertQuery(df):
    columns = list(df.columns.values)
    for i in range(len(columns)-1):
        columns[i] = str(columns[i]) + ','
    columns = stringFromList(columns)
    stringFormat = list('%s' for x in df.columns.values)
    for i in range(len(stringFormat)-1):
        stringFormat[i] = stringFormat[i] + ','
    stringFormat = stringFromList(stringFormat)
    return columns, stringFormat


def selectTickers(dbName, dbTable):
    conn = psycopg2.connect('dbname={} user=wesbdip password=waterloo2016'.format(dbName))
    cur = conn.cursor()
    qs = ('SELECT ticker FROM {};'.format(dbTable))
    cur.execute(qs)
    data = cur.fetchall()
    cols = [col[0] for col in cur.description]
    df = pd.DataFrame(data, columns=cols)
    cur.close()
    conn.close()
    return list(df.ticker.values)


# Inserts a dataframe into a SQL table.
def insertQuery(df, dbName, dbTable, dbUser, dbPassword):
    conn = psycopg2.connect('dbname={} user={} password={}'.format(dbName, dbUser, dbPassword))
    cur = conn.cursor()
    columns, stringFormat = stringInsertQuery(df)
    existTickers = selectTickers(dbName, dbTable)
    validTickers = [i for i in df.index.values if i not in existTickers]
    print(len(df), len(validTickers))
    for i in validTickers:
        rowValues = []
        for c in df.columns.values:
            if df.loc[i, c] == 'f':
                rowValues.append(False)
            elif df.loc[i, c] == 't':
                rowValues.append(True)
            elif pd.isnull(df.loc[i, c]):
                rowValues.append(None)
            elif type(df.loc[i, c]) == np.int64:
                rowValues.append(int(df.loc[i, c]))
            elif type(df.loc[i, c]) == np.float64:
                rowValues.append(float(df.loc[i, c]))
            else:
                rowValues.append(df.loc[i, c])
        rowValues = tuple(rowValues)
        querystring = 'INSERT INTO ' + dbTable + ' ' + columns + ' VALUES ' + stringFormat
        print(querystring)
        cur.execute(querystring, rowValues)
    conn.commit()
    cur.close()
    conn.close()


insertQuery(df, dbName, dbTable, dbUser, dbPassword)
