#!/usr/bin/python3.6

import pandas as pd
import psycopg2
import numpy as np


def selectStockColumns(dbName, dbTable):
    numcols = ['qvgs']
    conn = psycopg2.connect('dbname={} user=centos password=waterloo2016'
                            .format(dbName))
    cur = conn.cursor()
    qs = ('SELECT ticker, qvgs FROM {};'
          .format(dbTable))
    cur.execute(qs)
    data = cur.fetchall()
    cols = [col[0] for col in cur.description]
    df = pd.DataFrame(data, columns=cols)
    for x in numcols:
        df[x] = df[x].astype(np.float64)
    df = df.set_index('ticker')
    cur.close()
    conn.close()
    return df


def selectSecColumns(dbName, dbTable):
    numcols = ['rel_100day_newton', 'abs_100day_newton', 'newton_plus',
               'newton_delta', 'weighted_alpha']
    conn = psycopg2.connect('dbname={} user=centos password=waterloo2016'
                            .format(dbName))
    cur = conn.cursor()
    qs = ('SELECT ticker, rel_100day_newton, abs_100day_newton, '
          'newton_plus, newton_delta, weighted_alpha, security_type FROM {};'.format(dbTable))
    cur.execute(qs)
    data = cur.fetchall()
    cols = [col[0] for col in cur.description]
    df = pd.DataFrame(data, columns=cols)
    for x in numcols:
        df[x] = df[x].astype(np.float64)
    df = df.set_index('ticker')
    cur.close()
    conn.close()
    return df


def setBuys(df):
    newton_med = df['newton_plus'].median()
    if newton_med < 10:
        newton_med = 10
    delta_mean = df['newton_delta'].mean()
    if delta_mean < 0:
        delta_mean = 0
    df2 = df.copy()
    df2 = df2.loc[df2['rel_100day_newton'] > 0]
    df2 = df2.loc[df2['abs_100day_newton'] > 0]
    df2 = df2.loc[df2['newton_plus'] >= newton_med]
    df2 = df2.loc[df2['newton_delta'] >= delta_mean]
    df2 = df2.loc[df2['weighted_alpha'] > 0]
    for x in df2.index.values:
        if df2.loc[x, 'security_type'] == 'ETF':
            df2.loc[x, 'iws_rating'] = 'BUY'
        elif df2.loc[x, 'security_type'] == 'Stock':
            if df2.loc[x, 'qvgs'] >= 40:
                df2.loc[x, 'iws_rating'] = 'BUY'
    df2 = df2.dropna(subset=['iws_rating'])
    for x in df2.index.values:
        df.loc[x, 'iws_rating'] = df2.loc[x, 'iws_rating']
    return df


def setSells(df):
    df2 = df.copy()
    df2 = df2.loc[df2['newton_plus'] < 9]
    df2 = df2.loc[df2['rel_100day_newton'] == 0]
    df2 = df2.loc[df2['abs_100day_newton'] == 0]
    df2['iws_rating'] = 'SELL'
    for x in df2.index.values:
        df.loc[x, 'iws_rating'] = df2.loc[x, 'iws_rating']
    return df


def updquery(dbname, tname, df, colname):
    conn = psycopg2.connect('dbname={} user=centos password=waterloo2016'.format(dbname))
    cur = conn.cursor()
    myupd = "UPDATE " + tname + " SET " + colname + " = %s WHERE ticker = %s"
    for x in df.index.values:
        val = str(df.loc[x, colname])
        cur.execute(myupd, (val, x))
    conn.commit()
    cur.close()
    conn.close()


if __name__ == '__main__':
    stocks = selectStockColumns('researchdb', 'stock')
    secs = selectSecColumns('researchdb', 'security')
    df = secs.merge(stocks, left_index=True, right_index=True, how='outer')
    df['iws_rating'] = ''
    df = setBuys(df)
    df = setSells(df)
    updquery('researchdb', 'security', df, 'iws_rating')
