#!/usr/bin/python3.6

import pandas as pd
import psycopg2

path = '/home/centos/temp_dumps/'
hold_file = path + 'holdings.csv'
trans_file = path + 'transactions.csv'


# Builds a list of tuples. Tickers found in lookup are appened as (ticker, 1).
# Tickers not found in lookup are appened as (ticker, 0).
def checkmembership(tickers, lookup):
    output = []
    for x in tickers:
        if x in lookup:
            output.append((x, True))
        else:
            output.append((x, False))
    return output


# Returns a list of tickers from the database table.
def query_list(tname):
    conn = psycopg2.connect('dbname=researchdb user=centos password=waterloo2016')
    cur = conn.cursor()
    mysel = "SELECT ticker FROM "
    mysel = mysel + tname
    cur.execute(mysel)
    queryset = cur.fetchall()
    tickers = [x for y in queryset for x in y]
    cur.close()
    conn.close()
    return tickers


# Updates a column in a database table from a pandas dataframe.
def updquery(dbname, tname, df, colname):
    conn = psycopg2.connect('dbname={} user=centos password=waterloo2016'.format(dbname))
    cur = conn.cursor()
    myupd = "UPDATE " + tname + " SET " + colname + " = %s WHERE ticker = %s"
    for x in df.index.values:
        val = bool(df.loc[x, colname])
        cur.execute(myupd, (val, x))
    conn.commit()
    cur.close()
    conn.close()


# Searches for etfs and stocks a dataframe of holdings, updates the owned column in the database.
def findowned(tickers):
    dfhold = pd.read_csv(hold_file, skiprows=7)
    cols = ['Symbol/CUSIP', 'Market Value']
    dfhold = dfhold[cols]
    dfhold = dfhold.rename(columns={'Symbol/CUSIP': 'ticker', 'Market Value': 'market_value'})
    dfhold['ticker'] = (dfhold['ticker'].replace('BRKB', 'BRK-B', regex=True)
                                        .replace('RDSA', 'RDS-A', regex=True)
                                        .replace('BFB', 'BF-B', regex=True))
    dfhold['market_value'] = (dfhold['market_value'].replace('[\$,)]','', regex=True)
                                                    .replace('[(]','-', regex=True).astype(float))
    dfhold = dfhold.loc[dfhold['market_value'] > 1000.0]
    hold_ticks = list(dfhold['ticker'].unique())
    owned_tickers = checkmembership(tickers, hold_ticks)
    df_owned = pd.DataFrame(owned_tickers, columns=['ticker', 'owned'])
    df_owned = df_owned.set_index('ticker')
    updquery('researchdb', 'security', df_owned, 'owned')


# Searches for etfs and stocks a dataframe of transactions, updates the sold column in the database.
def findsold(tickers):
    dftrans = pd.read_csv(trans_file, skiprows=4)
    cols = ['Symbol/CUSIP', 'Action']
    dftrans = dftrans[cols]
    dftrans = dftrans.rename(columns={'Symbol/CUSIP': 'ticker'})
    dftrans['ticker'] = (dftrans['ticker'].replace('BRKB', 'BRK-B', regex=True)
                                        .replace('RDSA', 'RDS-A', regex=True)
                                        .replace('BFB', 'BF-B', regex=True))
    dftrans = dftrans.loc[dftrans['Action'] == 'Sell']
    sold_ticks = list(dftrans['ticker'].unique())
    sold_secs = checkmembership(tickers, sold_ticks)
    df_sold_sec = pd.DataFrame(sold_secs, columns=['ticker', 'sold_30'])
    df_sold_sec = df_sold_sec.set_index('ticker')
    updquery('researchdb', 'security', df_sold_sec, 'sold_30')


if __name__ == '__main__':
    tickers = query_list('security')
    findowned(tickers)
    findsold(tickers)
