#!/usr/bin/python3.6

import pandas as pd
import numpy as np
import psycopg2

path = '/home/wesbdip/temp_dumps/'

outFile = str(input('Enter filename of the csv you want to create: '))
dbName = str(input('Enter the name of the target database: '))
dbTable = str(input('Enter the name of the target table: '))
fName = path + outFile


def selectAllQuery(dbName, dbTable):
    conn = psycopg2.connect('dbname={} user=wesbdip password=waterloo2016'.format(dbName))
    cur = conn.cursor()
    cur.execute('SELECT * FROM {};'.format(dbTable))
    data = cur.fetchall()
    cols = [col[0] for col in cur.description]
    df = pd.DataFrame(data, columns=cols)
    cur.close()
    conn.close()
    return df


df = selectAllQuery(dbName, dbTable)
print(df.head())
df.to_csv(fName)
