#!/usr/bin/python3.6

import pandas as pd
import numpy as np
import psycopg2

path = '/home/centos/temp_dumps/'
stock_file = path + 'stock_barchart.csv'
etf_file = path + 'etf_barchart.csv'
qvg_file = path + 'qvg.csv'
greenblatt = path + 'greenblatt.csv'
guru = path + 'guru.csv'
#sects = path + 'stock_add_sector.csv'
invest = path + 'etf_investability.csv'
stock_sctr = path + 'stock_sctr.csv'
etf_sctr = path + 'etf_sctr.csv'

df_stock = pd.read_csv(stock_file)
df_etf = pd.read_csv(etf_file)
df_qvg = pd.read_csv(qvg_file)
df_green = pd.read_csv(greenblatt)
df_guru = pd.read_csv(guru)
#df_sects = pd.read_csv(sects)
df_invest = pd.read_csv(invest)
df_stock_sctr = pd.read_csv(stock_sctr)
df_etf_sctr = pd.read_csv(etf_sctr)


def convertPercents(per):
    if type(per) == str:
        per = np.round((float(per[:-1]) / 100.0), 4)
        return per
    else:
        return per

    
def cleanBC(df, mapper):
    df = df.rename(mapper, axis=1)
    keepCols = list(mapper.values())
    df = df[[x for x in df.columns.values if x in keepCols]]
    pctCols = ['volatility', 'div_yield']
    for c in pctCols:
        if c in df.columns.values:
            df[c] = df[c].apply(convertPercents)
    df = (df.replace('unch', 'null')
          .replace('N/A', 'null').fillna('null'))
    return df

def cleanNa(df):
    df = (df.replace('unch', 'null')
          .replace('N/A', 'null').fillna('null'))
    return df

bcMapper = {'Symbol': 'ticker', ' Symbol': 'ticker', 'Wtd Alpha': 'weighted_alpha',
            '100D His Vol': 'volatility', 'Opinion': 'analysts_opinion', 'Long Term': 'lt_opinion',
            'Div Yield': 'div_yield', 'qvg': 'qvgs', 'WtdAlpha': 'weighted_alpha', ' WtdAlpha': 'weighted_alpha',
            ' Opinion': 'analysts_opinion', 'LongTerm': 'lt_opinion', 'DivYield': 'div_yield',
            '100DHisVol': 'volatility', 'Market Cap': 'market_cap'}

df_etf = cleanBC(df_etf, bcMapper)
df_stock = cleanBC(df_stock, bcMapper)
df_stock = df_stock.set_index('ticker')
df_etf = df_etf.set_index('ticker')
df_qvg = df_qvg.set_index('ticker')
df_qvg = df_qvg.rename(columns={'qvg': 'qvgs', 'mmr': 'qvgs'})
df_green = df_green.set_index('ticker')
df_guru = cleanNa(df_guru)
df_guru = df_guru.set_index('ticker')
#df_sects = df_sects.set_index('ticker')
df_invest = df_invest.set_index('ticker')
df_stock_sctr = df_stock_sctr.set_index('ticker')
df_etf_sctr = df_etf_sctr.set_index('ticker')
# df_stock = df_stock.drop(['market_cap'], axis=1)
# df_etf = df_etf.drop(['market_cap'], axis=1)

typeDict = {'weighted_alpha': float, 'market_cap': int,
            'div_yield': float, 'volatility': float,
            'analysts_opinion': str, 'qvgs': float,
            'greenblatt': float, 'qvg': float,
            'quality': float, 'value': float, 'growth': float,
            'lt_opinion': str, 'altman_zscore': float,
            'piotroski_fscore': int, 'sp500': int,
            'sector': str, 'investability_score': float,
            'industry': str, 'sentiment': float, 'investable': bool,
            'sctr': float}


def updall(dbname, tname, df):
    conn = psycopg2.connect('dbname={} user=centos password=waterloo2016'.format(dbname))
    cur = conn.cursor()
    for col in df.columns.values:
        myupd = "UPDATE " + tname + " SET " + col + " = %s WHERE ticker = %s"
        for tick in df.index.values:
            if df.loc[tick, col] != 'null':
                try:
                    val = typeDict[col](df.loc[tick, col])
                    print(tick, col, val)
                    cur.execute(myupd, (val, tick))
                except Exception as ex:
                    print('Error on:')
                    val = typeDict[col](df.loc[tick, col])
                    print(tick, val, type(val))
                    print(ex)
                    quit()
    conn.commit()
    cur.close()
    conn.close()


# updall('researchdb', 'security', df_etf)
# updall('researchdb', 'security', df_stock)
updall('researchdb', 'stock', df_qvg)
# updall('researchdb', 'stock', df_green)
# updall('researchdb', 'stock', df_guru)
# updall('researchdb', 'stock', df_sects)
# updall('researchdb', 'etf', df_invest)
# updall('researchdb', 'security', df_etf_sctr)
# updall('researchdb', 'security', df_stock_sctr)
