#!/usr/bin/python3.6

import pandas as pd
import psycopg2

path = '/home/wesbdip/temp_dumps/'
etf_file = path + 'etfs_no_cat.csv'

df_etf = pd.read_csv(etf_file)
df_etf = df_etf.set_index('ticker')


def updall(dbname, tname, df):
    conn = psycopg2.connect('dbname={} user=wesbdip password=waterloo2016'.format(dbname))
    cur = conn.cursor()
    for col in df.columns.values:
        myupd = "UPDATE " + tname + " SET " + col + " = %s WHERE ticker = %s"
        for tick in df.index.values:
            if df.loc[tick, col] != 'null':
                try:
                    val = df.loc[tick, col]
                    print(tick, col, val)
                    cur.execute(myupd, (val, tick))
                except Exception as ex:
                    print('Error on:')
                    # print(tick, val, type(val))
                    print(ex)
                    quit()
    conn.commit()
    cur.close()
    conn.close()


updall('modeldb', 'etf_universe', df_etf)
