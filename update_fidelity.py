#!/usr/bin/python3.6

import pandas as pd
import psycopg2

path = '/home/wesbdip/temp_dumps/'
fidelity_file = path + 'fidelity.csv'
df_fidelity = pd.read_csv(fidelity_file)


def selectEtfTickers():
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    selectQuery = "SELECT ticker FROM etf_universe"
    cur.execute(selectQuery)
    queryset = cur.fetchall()
    dbTickers = [x for y in queryset for x in y]
    conn.commit()
    cur.close()
    conn.close()
    return dbTickers


def updateFidelity(df, dbTickers):
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    updateQuery = "UPDATE etf_universe SET fidelity_comm_free = %s WHERE ticker = %s"
    for tick in df.ticker.values:
        cur.execute(updateQuery, (True, tick))
    for dbt in dbTickers:
        if dbt not in df.ticker.values:
            cur.execute(updateQuery, (False, tick))
    conn.commit()
    cur.close()
    conn.close()
    

dbTickers = selectEtfTickers()
updateFidelity(df_fidelity, dbTickers)
