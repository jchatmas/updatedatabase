#!/usr/bin/python3.6

import pandas as pd
import psycopg2

path = '/home/wesbdip/temp_dumps/'
schwab_file = path + 'onesource.csv'
df_schwab = pd.read_csv(schwab_file)


def selectEtfTickers():
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    selectQuery = "SELECT ticker FROM etf_universe"
    cur.execute(selectQuery)
    queryset = cur.fetchall()
    dbTickers = [x for y in queryset for x in y]
    conn.commit()
    cur.close()
    conn.close()
    return dbTickers


def updateSchwab(df, dbTickers):
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    updateQuery = "UPDATE etf_universe SET schwab_onesource = %s WHERE ticker = %s"
    for tick in df.ticker.values:
        cur.execute(updateQuery, (True, tick))
    for dbt in dbTickers:
        if dbt not in df.ticker.values:
            cur.execute(updateQuery, (False, tick))
    conn.commit()
    cur.close()
    conn.close()
    

def checkDatabaseMembership(df, dbTickers):
    uncoveredTickers = []
    for tick in df.ticker.values:
        if tick not in dbTickers:
            tick = (tick,)
            uncoveredTickers.append(tick)
    uncoveredFrame = pd.DataFrame(uncoveredTickers, columns=['ticker'])
    return uncoveredFrame


dbTickers = selectEtfTickers()
updateSchwab(df_schwab, dbTickers)
uncoveredFrame = checkDatabaseMembership(df_schwab, dbTickers)
print(len(uncoveredFrame), ' uncovered tickers. Results written to file.')
fname = path + 'uncovered_schwab.csv'
uncoveredFrame.to_csv(fname)
    
