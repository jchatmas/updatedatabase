#!/usr/bin/python3.6

import pandas as pd
import psycopg2

path = '/home/wesbdip/temp_dumps/'
sp_file = path + 'sp500_tickers.csv'
df_sp = pd.read_csv(sp_file)


def selectStockTickers():
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    selectQuery = "SELECT ticker FROM stock_universe"
    cur.execute(selectQuery)
    queryset = cur.fetchall()
    dbTickers = [x for y in queryset for x in y]
    conn.commit()
    cur.close()
    conn.close()
    return dbTickers


def updateSp(df, dbTickers):
    conn = psycopg2.connect('dbname=modeldb user=wesbdip password=waterloo2016')
    cur = conn.cursor()
    updateQuery = "UPDATE stock_universe SET sp500 = %s WHERE ticker = %s"
    for tick in df.ticker.values:
        cur.execute(updateQuery, (1, tick))
    for dbt in dbTickers:
        if dbt not in df.ticker.values:
            cur.execute(updateQuery, (0, dbt))
    conn.commit()
    cur.close()
    conn.close()


dbTickers = selectStockTickers()
updateSp(df_sp, dbTickers)
